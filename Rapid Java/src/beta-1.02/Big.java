package RapidJava;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Big {
	//declaration of a ZERO BigDecimal variable
	public BigDecimal bd = BigDecimal.ZERO;
	
	//casting from double
	public BigDecimal bd(double object) {
		return BigDecimal.valueOf(object);
	}
	
	//casting from int
	public BigDecimal bd(int object) {
		return BigDecimal.valueOf(object);
	}
	
	//casting from float
	public BigDecimal bd(float object) {
		return BigDecimal.valueOf(object);
	}
	
	//casting from short
	public BigDecimal bd(short object) {
		return BigDecimal.valueOf(object);
	}
	
	//casting from long
	public BigDecimal bd(long object) {
		return BigDecimal.valueOf(object);
	}
	
	//casting from String 
	public BigDecimal bd(String object) {
		return BigDecimal.valueOf(Float.parseFloat(object));
	}
	
	//addition
	public BigDecimal add(BigDecimal bd1, BigDecimal bd2) {
		return bd1.add(bd2);
	}
	
	//subtraction
	public BigDecimal sub(BigDecimal bd1, BigDecimal bd2) {
		return bd1.subtract(bd2);
	}
	
	//multiplication
	public BigDecimal mul(BigDecimal bd1, BigDecimal bd2) {
		return bd1.multiply(bd2);
	}
	
	//division
	public BigDecimal div(BigDecimal bd1, BigDecimal bd2) {
		return bd1.divide(bd2);
	}
	
	//log
	public int log(BigDecimal bd, BigDecimal base) {
		int n = 0;
		while (true) {
			n += 1;
			if (base.pow(n).equals(bd)) {
				break;
			} else if (base.pow(n).abs().compareTo(bd.abs()) > 0){
				n = 0;
				break;
			} else if (n > 100) {
				n = 0;
				break;
			}
		}
		return n;
	}
	
	//comparison
	public float dif(BigDecimal object_1, BigDecimal object_2) {
		return (object_1.subtract(object_2).floatValue());
	}
	
	//comparison: if bigger 
	public boolean bgr(BigDecimal object_1, BigDecimal object_2) {
		if (object_1.compareTo(object_2) > 0){
			return true;
		} else {
			return false;
		}
	}
	
	//comparison: if smaller 
	public boolean smr(BigDecimal object_1, BigDecimal object_2) {
		if (object_1.compareTo(object_2) < 0){
			return true;
		} else {
			return false;
		}
	}
	
	//comparison: if equaled 
	public boolean eql(BigDecimal object_1, BigDecimal object_2) {
		if (object_1.equals(object_2)){
			return true;
		} else {
			return false;
		}
	}
	
	//comparison: if zero
	public boolean isZero(BigDecimal object) {
		if (object.compareTo(BigDecimal.ZERO) == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	//rounding decimals
	public BigDecimal rnd(BigDecimal object, int digit) {
		return object.setScale(digit, RoundingMode.HALF_UP);
	}
	
	//rounding-up decimals
	public BigDecimal rndup(BigDecimal object, int digit) {
		return object.setScale(digit, RoundingMode.UP);
	}
	
	//rounding-down decimals
	public BigDecimal rnddn(BigDecimal object, int digit) {
		return object.setScale(digit, RoundingMode.DOWN);
	}
}
