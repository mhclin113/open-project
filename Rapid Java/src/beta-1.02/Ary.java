package RapidJava;

import java.util.ArrayList;

public class Ary {
	//declaration of an ArrayList<Object> with no values inside
	public ArrayList<Object> al = new ArrayList<>();
	
	//declaration of an ArrayList<Double> with no values inside
	public ArrayList<Double> al_d = new ArrayList<>();
	
	//declaration of an ArrayList<Float> with no values inside
	public ArrayList<Float> al_f = new ArrayList<>();
	
	//declaration of an ArrayList<Integer> with no values inside
	public ArrayList<Integer> al_i = new ArrayList<>();
	
	//declaration of an ArrayList<Long> with no values inside
	public ArrayList<Long> al_l = new ArrayList<>();
	
	//declaration of an ArrayList<String> with no values inside
	public ArrayList<String> al_s = new ArrayList<>();
	
	//declaration of an ArrayList<Boolean> with no values inside
	public ArrayList<Boolean> al_b = new ArrayList<>();
	
	//merger
	public ArrayList<Object> merge(ArrayList<Object> objects_1, ArrayList<Object> objects_2){
		ArrayList<Object> result = new ArrayList<>();
		result.addAll(objects_1);
		result.addAll(objects_2);
		return result;
	}
	
	//casting ArrayList<String> to ArrayList<Integer>
	public ArrayList<Integer> itg(ArrayList<String> objects){
		ArrayList<Integer> result = new ArrayList<>();
		for (String object:objects) {
			result.add(Integer.parseInt(object));
		}
		return result;
	}
	
	//casting ArrayList<all types> to ArrayList<String>
	public ArrayList<String> str(ArrayList<Object> objects){
		ArrayList<String> results = new ArrayList<>();
		for (Object object : objects) {
			results.add(String.valueOf(object));
		}
		return results;
	}
	
	//loop with interval
	public ArrayList<Object> intrv(ArrayList<Object> object, int interval) {
		ArrayList<Object> result = new ArrayList<>();
		for (int idx = 0; idx < object.size(); idx+=interval) {
			result.add(object.get(idx));
		}
		return result;
	}
	
	//fetching the maximum value in ArrayList<Double>
	public double max_d(ArrayList<Double> objects) {
		double result = 0;
		for (double object : objects) {
			if (result < object) {
				result = object;
			}
		}
		return result;
	}

	//fetching the maximum value in ArrayList<Float>
	public float max_f(ArrayList<Float> objects) {
		float result = 0;
		for (float object : objects) {
			if (result < object) {
				result = object;
			}
		}
		return result;
	}

	//fetching the maximum value in ArrayList<Integer>
	public int max_i(ArrayList<Integer> objects) {
		int result = 0;
		for (int object : objects) {
			if (result < object) {
				result = object;
			}
		}
		return result;
	}

	//fetching the maximum value in ArrayList<Long>
	public long max_l(ArrayList<Long> objects) {
		long result = 0;
		for (long object : objects) {
			if (result < object) {
				result = object;
			}
		}
		return result;
	}
	
	//fetching the minimum value in ArrayList<Double>
	public double min_d(ArrayList<Double> objects) {
		double result = 0;
		for (double object : objects) {
			if (result > object) {
				result = object;
			}
		}
		return result;
	}
	
	//fetching the minimum value in ArrayList<Float>
	public float min_f(ArrayList<Float> objects) {
		float result = 0;
		for (float object : objects) {
			if (result > object) {
				result = object;
			}
		}
		return result;
	}

	//fetching the minimum value in ArrayList<Integer>
	public int min_i(ArrayList<Integer> objects) {
		int result = 0;
		for (int object : objects) {
			if (result > object) {
				result = object;
			}
		}
		return result;
	}

	//fetching the minimum value in ArrayList<Long>
	public long min_l(ArrayList<Long> objects) {
		long result = 0;
		for (long object : objects) {
			if (result > object) {
				result = object;
			}
		}
		return result;
	}
	
	//summation of ArrayList<Double>
	public double sum_d(ArrayList<Double> objects) {
		double result = 0;
		for (double object : objects) {
			result += object;
		}
		return result;
	}
	
	//summation of ArrayList<Float>
	public float sum_f(ArrayList<Float> objects) {
		float result = 0;
		for (float object : objects) {
			result += object;
		}
		return result;
	}
	
	//summation of ArrayList<Integer>
	public int sum_i(ArrayList<Integer> objects) {
		int result = 0;
		for (int object : objects) {
			result += object;
		}
		return result;
	}
	
	//summation of ArrayList<Long>
	public long sum_l(ArrayList<Long> objects) {
		long result = 0;
		for (long object : objects) {
			result += object;
		}
		return result;
	}
	
	//fetching the median of ArrayList<Double>
	public double med_d(ArrayList<Double> objects) {
		int spare = objects.size()%2;
		if (spare == 0) {
			double pre = objects.get(objects.size()/2-1);
			double post = objects.get(objects.size()/2);
			return (pre+post)/2;
		} else {
			int index = objects.size()/2;
			return (objects.get(index));
		}
	}
	
	//fetching the median of ArrayList<Float>
	public double med_f(ArrayList<Float> objects) {
		int spare = objects.size()%2;
		if (spare == 0) {
			float pre = objects.get(objects.size()/2-1);
			float post = objects.get(objects.size()/2);
			return (pre+post)/2;
		} else {
			int index = objects.size()/2;
			return (objects.get(index));
		}
	}
	
	//fetching the median of ArrayList<Integer>
	public double med_i(ArrayList<Integer> objects) {
		int spare = objects.size()%2;
		if (spare == 0) {
			int pre = objects.get(objects.size()/2-1);
			int post = objects.get(objects.size()/2);
			return (pre+post)/2;
		} else {
			int index = objects.size()/2;
			return (objects.get(index));
		}
	}
	
	//fetching the median of ArrayList<Long>
	public double med_l(ArrayList<Long> objects) {
		int spare = objects.size()%2;
		if (spare == 0) {
			long pre = objects.get(objects.size()/2-1);
			long post = objects.get(objects.size()/2);
			return (pre+post)/2;
		} else {
			int index = objects.size()/2;
			return (objects.get(index));
		}
	}
	
	//calculating the average of ArrayList<Double>
	public double avg_d(ArrayList<Double> objects) {
		int size = objects.size();
		return sum_d(objects)/size;
	}

	//calculating the average of ArrayList<Float>
	public double avg_f(ArrayList<Float> objects) {
		int size = objects.size();
		return sum_f(objects)/size;
	}
	
	//calculating the average of ArrayList<Integer>
	public double avg_i(ArrayList<Integer> objects) {
		int size = objects.size();
		return sum_i(objects)/size;
	}
	
	//calculating the average of ArrayList<Long>
	public double avg_l(ArrayList<Long> objects) {
		int size = objects.size();
		return sum_l(objects)/size;
	}
}
