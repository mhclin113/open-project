package RapidJava;

import java.time.Year;

public class Tme {
	//fetching current time in nanosecond
	public long nano() {
		return System.nanoTime();
	}
	
	//fetching current time in millisecond
	public long mili() {
		return System.currentTimeMillis();
	}
	
	//checking a year is leap or not
	public boolean leap(int year) {
		return Year.of(year).isLeap();
	}
}
