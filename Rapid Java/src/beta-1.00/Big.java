package RapidJava;

import java.math.BigDecimal;

public class Big {
	//declaration of a ZERO BigDecimal variable
	public BigDecimal bd = BigDecimal.ZERO;
	
	//casting from double
	public BigDecimal bd(double object) {
		return BigDecimal.valueOf(object);
	}
	
	//casting from int
	public BigDecimal bd(int object) {
		return BigDecimal.valueOf(object);
	}
	
	//casting from float
	public BigDecimal bd(float object) {
		return BigDecimal.valueOf(object);
	}
	
	//casting from short
	public BigDecimal bd(short object) {
		return BigDecimal.valueOf(object);
	}
	
	//casting from long
	public BigDecimal bd(long object) {
		return BigDecimal.valueOf(object);
	}
	
	//casting from String 
	public BigDecimal bd(String object) {
		return BigDecimal.valueOf(Float.parseFloat(object));
	}
	
	//addition
	public BigDecimal add(BigDecimal bd1, BigDecimal bd2) {
		return bd1.add(bd2);
	}
	
	//subtraction
	public BigDecimal sub(BigDecimal bd1, BigDecimal bd2) {
		return bd1.subtract(bd2);
	}
	
	//multiplication
	public BigDecimal mul(BigDecimal bd1, BigDecimal bd2) {
		return bd1.multiply(bd2);
	}
	
	//division
	public BigDecimal div(BigDecimal bd1, BigDecimal bd2) {
		return bd1.divide(bd2);
	}
	
	//comparison
	public float dif(BigDecimal object_1, BigDecimal object_2) {
		return (object_1.subtract(object_2).floatValue());
	}

}
