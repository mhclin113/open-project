package RapidJava;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

public class Fnc {
	//checking type of an object
	public String type(Object object) {
		if (object instanceof Integer) {
			return "Integer";
		} else if (object instanceof String) {
			return "String";
		} else if (object instanceof BigDecimal) {
			return "BigDecimal";
		} else if (object instanceof ArrayList) {
			return "ArrayList";
		} else if (object instanceof Date) {
			return "Date";
		} else if (object instanceof Float) {
			return "Float";
		} else if (object instanceof Long) {
			return "Long";
		} else if (object instanceof Character) {
			return "Character";
		} else if (object instanceof Double) {
			return "Double";
		} else if (object instanceof Byte) {
			return "Byte";
		} else if (object instanceof Short) {
			return "Short";
		} else if (object instanceof Boolean) {
			return "Boolean";
		} else {
			return "unknown type";
		}
	}
	
	//casting String to int
	public int itg(String object) {
		return Integer.parseInt(object);
	} 
	
	//casting char to int
	public int itg(char object) {
		return Character.getNumericValue(object);
	} 
	
	//casting BigDecimal to int
	public int itg(BigDecimal object) {
		return object.intValue();
	}
	
	//casting all types to String
	public String str(Object object) {
		return String.valueOf(object);
	}
	
	//printing object to console
	public void prt(Object object) {
		System.out.print(object);
	}
	
	//printing object and next line to console
	public void prtl(Object object) {
		System.out.println(object);
	}
	
	//getting current operating system
	public String os() {
		return System.getProperty("os.name");
	}
}
