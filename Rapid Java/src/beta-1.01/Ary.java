package RapidJava;

import java.util.ArrayList;

public class Ary {
	//declaration of an ArrayList with no values inside
	public ArrayList<Object> al = new ArrayList<>();
	
	//merger
	public ArrayList<Object> merge(ArrayList<Object> objects_1, ArrayList<Object> objects_2){
		ArrayList<Object> result = new ArrayList<>();
		result.addAll(objects_1);
		result.addAll(objects_2);
		return result;
	}
	
	//casting ArrayList<String> to ArrayList<Integer>
	public ArrayList<Integer> itg(ArrayList<String> objects){
		ArrayList<Integer> result = new ArrayList<>();
		for (String object:objects) {
			result.add(Integer.parseInt(object));
		}
		return result;
	}
	
	//casting ArrayList<all types> to ArrayList<String>
	public ArrayList<String> str(ArrayList<Object> objects){
		ArrayList<String> results = new ArrayList<>();
		for (Object object : objects) {
			results.add(String.valueOf(object));
		}
		return results;
	}
	
	//loop with interval
	public ArrayList<Object> intrv(ArrayList<Object> object, int interval) {
		ArrayList<Object> result = new ArrayList<>();
		for (int idx = 0; idx < object.size(); idx+=interval) {
			result.add(object.get(idx));
		}
		return result;
	}
	
	//fetching the maximum value
	public double max(ArrayList<Double> objects) {
		double result = 0;
		for (double object : objects) {
			if (result < object) {
				result = object;
			}
		}
		return result;
	}
	
	//fetching the minimum value
	public double min(ArrayList<Double> objects) {
		double result = 0;
		for (double object : objects) {
			if (result > object) {
				result = object;
			}
		}
		return result;
	}
	
	//summation
	public double sum(ArrayList<Double> objects) {
		double result = 0;
		for (double object : objects) {
			result += object;
		}
		return result;
	}
	

}
