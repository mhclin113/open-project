package RapidJava;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Year;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

public class Tme {
	//fetching current date in LocalDate
	public LocalDate local() {
		return LocalDate.now();
	}
	
	//fetching current date in java.sql.Date
	public Date sql() {
		return new Date(System.currentTimeMillis());
	}
	
	//fetching current date in java.util.Date
	public java.util.Date util(){
		return new java.util.Date(System.currentTimeMillis());
	}
	
	//fetching current time in nanosecond
	public long nano() {
		return System.nanoTime();
	}
	
	//fetching current time in millisecond
	public long mili() {
		return System.currentTimeMillis();
	}
	
	//checking a year is leap or not
	public boolean leap(int year) {
		return Year.of(year).isLeap();
	}
	
	//day subtraction of two dates in LocalDate
	public long subDay(LocalDate date_1, LocalDate date_2) {
		return ChronoUnit.DAYS.between(date_2, date_1);
	}
	
	//day subtraction of two dates in String
	public long subDay(String date_1, String date_2) {
		return ChronoUnit.DAYS.between(LocalDate.parse(date_2), LocalDate.parse(date_1));
	}
	
	//day subtraction of two dates in java.sql.Date
	public long subDay(Date date_1, Date date_2) {
		return ChronoUnit.DAYS.between(date_2.toLocalDate(), date_1.toLocalDate());
	}
	
	//day subtraction of two dates in java.util.Date
	public long subDay(java.util.Date date_1, java.util.Date date_2) {
		return ChronoUnit.DAYS.between(new Date((date_2).getTime()).toLocalDate(), new Date((date_1).getTime()).toLocalDate());
	}
	
	//day calculation
	public Object calcDay(Object date, int days) {
		if (date instanceof LocalDate) {
			return ((LocalDate) date).plusDays(days);
		} else if (date instanceof Date) {
			Calendar cal = Calendar.getInstance();
			cal.setTime((Date)date);
			cal.add(Calendar.DATE, days);
			return new Date(cal.getTime().getTime());
		} else if (date instanceof java.util.Date) {
			Calendar cal = Calendar.getInstance();
			cal.setTime((java.util.Date)date);
			cal.add(Calendar.DATE, days);
			return cal.getTime();
		} else {
			return null;
		}
	}
	
	//month calculation
	public Object calcMon(Object date, int months) {
		if (date instanceof LocalDate) {
			return ((LocalDate) date).plusMonths(months);
		} else if (date instanceof Date) {
			Calendar cal = Calendar.getInstance();
			cal.setTime((Date)date);
			cal.add(Calendar.MONTH, months);
			return new Date(cal.getTime().getTime());
		} else if (date instanceof java.util.Date) {
			Calendar cal = Calendar.getInstance();
			cal.setTime((java.util.Date)date);
			cal.add(Calendar.MONTH, months);
			return cal.getTime();
		} else {
			return null;
		}
	}
	
	//year calculation
	public Object calcYear(Object date, int years) {
		if (date instanceof LocalDate) {
			return ((LocalDate) date).plusYears(years);
		} else if (date instanceof Date) {
			Calendar cal = Calendar.getInstance();
			cal.setTime((Date)date);
			cal.add(Calendar.YEAR, years);
			return new Date(cal.getTime().getTime());
		} else if (date instanceof java.util.Date) {
			Calendar cal = Calendar.getInstance();
			cal.setTime((java.util.Date)date);
			cal.add(Calendar.YEAR, years);
			return cal.getTime();
		} else {
			return null;
		}
	}
}
